package com.example.examensrpago.data.vendor.model

import com.google.gson.annotations.SerializedName

data class VendorDetail(
    val activo: Boolean,
    val email: String,
    val nickname: String,
    val nombre: String,
    @SerializedName("primer-apellido")
    val primer_apellido: String,
    @SerializedName("segundo-apellido")
    val segundo_apellido: String
)