package com.example.examensrpago.data.car.repository

import com.example.examensrpago.data.car.model.Cars

interface Repository {
    suspend fun getCars():Cars
}