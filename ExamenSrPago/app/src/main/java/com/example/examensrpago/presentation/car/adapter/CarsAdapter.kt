package com.example.examensrpago.presentation.car.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.examensrpago.data.car.model.Auto

import com.example.examensrpago.databinding.ItemCarsBinding
import com.example.examensrpago.presentation.base.BaseViewHolder

class CarsAdapter(private val list: List<Auto>,private val carsclickListener: OnCarsclickListener):RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnCarsclickListener{
        fun onclickMovie()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding=ItemCarsBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val movieviewHolder = CarsViewHolder(itemBinding)
        itemBinding.root.setOnClickListener{
            carsclickListener.onclickMovie()
        }

        return movieviewHolder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {

        when(holder){
            is CarsViewHolder -> holder.bind(list[position])
        }
    }

    class CarsViewHolder(val binding: ItemCarsBinding):BaseViewHolder<Auto>(binding.root){
        override fun bind(item: Auto) {
         binding.idMarca.text= item.marca
         binding.idModelo.text = item.modelo
         binding.idPrecio.text = item.precio

        }

    }

    override fun getItemCount(): Int = list.size

}