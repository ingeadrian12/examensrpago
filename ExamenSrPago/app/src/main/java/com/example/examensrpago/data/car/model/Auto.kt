package com.example.examensrpago.data.car.model

data class Auto(
    val id: String,
    val marca: String,
    val modelo: String,
    val precio: String
)