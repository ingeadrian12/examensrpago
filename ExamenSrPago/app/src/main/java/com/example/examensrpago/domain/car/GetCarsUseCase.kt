package com.example.examensrpago.domain.car

import com.example.examensrpago.data.car.model.Cars
import com.example.examensrpago.data.car.repository.RepositoryImpl

class GetCarsUseCase(private val repositoryImpl: RepositoryImpl) {
    suspend fun getCars():Cars = repositoryImpl.getCars()
}