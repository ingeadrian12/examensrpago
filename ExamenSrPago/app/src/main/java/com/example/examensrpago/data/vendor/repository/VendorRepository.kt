package com.example.examensrpago.data.vendor.repository

import com.example.examensrpago.data.vendor.model.VendorDetail

interface VendorRepository {
    suspend fun getVendorDetail():VendorDetail
}