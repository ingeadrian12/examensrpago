package com.example.examensrpago.presentation.vendor.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.examensrpago.R
import com.example.examensrpago.data.api.RetrofitClient
import com.example.examensrpago.data.vendor.remote.RemoteVendorDetailSource
import com.example.examensrpago.data.vendor.repository.VendorRepositoryImpl
import com.example.examensrpago.databinding.FragmentVendorBinding
import com.example.examensrpago.domain.vendor.GetDetailVendorUsecase
import com.example.examensrpago.presentation.base.Resource
import com.example.examensrpago.presentation.vendor.viewmodel.VendorViewModel


class VendorFragment : Fragment(R.layout.fragment_vendor) {
    private lateinit var binding: FragmentVendorBinding
    private val viewmodel by viewModels<VendorViewModel> {
        VendorViewModel.VendorViewModelFactory(GetDetailVendorUsecase(VendorRepositoryImpl(
            RemoteVendorDetailSource(RetrofitClient.webservice)
        )))
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentVendorBinding.bind(view)
        viewmodel.getVendorDetailFetch().observe(viewLifecycleOwner, Observer {

            when(it){
                is Resource.Loading ->{

                }

                is Resource.Success ->{
                    binding.idNombre.text = it.data.nombre
                    binding.idApellido.text = it.data.primer_apellido
                    binding.idSegundoApel.text = it.data.segundo_apellido
                    binding.idEmail.text = it.data.email
                    binding.idNickName.text = it.data.nickname
                    binding.idStatus.text = it.data.activo.toString()
                }

                is  Resource.Failure ->{

                }
            }
        })

    }



}