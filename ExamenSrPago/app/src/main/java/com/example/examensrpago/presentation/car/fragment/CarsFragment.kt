package com.example.examensrpago.presentation.car.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.examensrpago.R
import com.example.examensrpago.data.api.RetrofitClient
import com.example.examensrpago.data.car.remote.RemoteCarsSource
import com.example.examensrpago.data.car.repository.RepositoryImpl
import com.example.examensrpago.databinding.FragmentCarsBinding
import com.example.examensrpago.domain.car.GetCarsUseCase
import com.example.examensrpago.presentation.base.Resource
import com.example.examensrpago.presentation.car.adapter.CarsAdapter
import com.example.examensrpago.presentation.car.viewmodel.CarsViewModel
import com.example.examensrpago.presentation.vendor.fragment.VendorFragment


class CarsFragment : Fragment(R.layout.fragment_cars),CarsAdapter.OnCarsclickListener {

    lateinit var binding: FragmentCarsBinding
    lateinit var carsAdapter: CarsAdapter
    private val viewModel by viewModels<CarsViewModel> {
        CarsViewModel.CarsViewModelFactory(GetCarsUseCase(RepositoryImpl(RemoteCarsSource(RetrofitClient.webservice))))
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCarsBinding.bind(view)
        viewModel.carsFetch().observe(viewLifecycleOwner, Observer {
            when(it){
                is Resource.Loading->{

                }

                is Resource.Success ->{
                    carsAdapter = CarsAdapter(it.data.autos,this)
                    binding.listCars.adapter = carsAdapter
                }
                is Resource.Failure ->{

                }
            }
        })
    }

    override fun onclickMovie() {
  val action =CarsFragmentDirections.actionCarsFragmentToVendorFragment()
        findNavController().navigate(action)
    }

}