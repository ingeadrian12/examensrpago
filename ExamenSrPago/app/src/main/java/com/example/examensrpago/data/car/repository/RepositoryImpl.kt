package com.example.examensrpago.data.car.repository

import com.example.examensrpago.data.car.model.Cars
import com.example.examensrpago.data.car.remote.RemoteCarsSource

class RepositoryImpl(private val remoteCarsSource: RemoteCarsSource):Repository {
    override suspend fun getCars(): Cars = remoteCarsSource.getCars()
}