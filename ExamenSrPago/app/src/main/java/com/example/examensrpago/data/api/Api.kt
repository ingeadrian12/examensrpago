package com.example.examensrpago.data.api

import com.example.examensrpago.data.car.model.Cars
import com.example.examensrpago.data.constants.Constants
import com.example.examensrpago.data.vendor.model.VendorDetail
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface Api{
    @GET("v3/8d1b1cb0-a8fc-41c1-a7a8-3bf1711c62ec")
    suspend fun getCars():Cars
    @GET("v3/bbca2fcb-381f-4fe8-8f7b-727ae5c70250")
    suspend fun getVendor():VendorDetail
}

object RetrofitClient{

    val webservice by lazy {
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(Api::class.java)
    }
}