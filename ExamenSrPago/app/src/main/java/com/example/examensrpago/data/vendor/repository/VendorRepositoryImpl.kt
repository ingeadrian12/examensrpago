package com.example.examensrpago.data.vendor.repository

import com.example.examensrpago.data.vendor.model.VendorDetail
import com.example.examensrpago.data.vendor.remote.RemoteVendorDetailSource

class VendorRepositoryImpl(private val remoteVendorDetailSource: RemoteVendorDetailSource):VendorRepository {
    override suspend fun getVendorDetail(): VendorDetail = remoteVendorDetailSource.getVendorDetail()



}