package com.example.examensrpago.data.vendor.remote

import com.example.examensrpago.data.api.Api
import com.example.examensrpago.data.vendor.model.VendorDetail

class RemoteVendorDetailSource(private val api: Api) {
    suspend fun getVendorDetail():VendorDetail = api.getVendor()
}