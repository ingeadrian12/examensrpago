package com.example.examensrpago.data.car.remote

import com.example.examensrpago.data.api.Api
import com.example.examensrpago.data.car.model.Cars

class RemoteCarsSource(private val api: Api) {
    suspend fun getCars():Cars = api.getCars()
}