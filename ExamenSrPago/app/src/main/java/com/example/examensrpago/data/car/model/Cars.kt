package com.example.examensrpago.data.car.model

data class Cars(
    val autos: List<Auto>
)