package com.example.examensrpago.presentation.car.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.example.examensrpago.domain.car.GetCarsUseCase
import com.example.examensrpago.presentation.base.Resource
import kotlinx.coroutines.Dispatchers

class CarsViewModel(private val getCarsUseCase: GetCarsUseCase):ViewModel() {

    fun carsFetch() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(getCarsUseCase.getCars()))

        }
        catch (ex:Exception){
            emit(Resource.Failure(ex))
        }
    }




    class CarsViewModelFactory(private val getCarsUseCase: GetCarsUseCase):ViewModelProvider.Factory{
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(GetCarsUseCase::class.java).newInstance(getCarsUseCase)
        }

    }
}