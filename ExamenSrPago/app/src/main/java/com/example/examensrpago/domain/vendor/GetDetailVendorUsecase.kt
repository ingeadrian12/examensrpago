package com.example.examensrpago.domain.vendor

import com.example.examensrpago.data.vendor.model.VendorDetail
import com.example.examensrpago.data.vendor.repository.VendorRepository
import com.example.examensrpago.data.vendor.repository.VendorRepositoryImpl

class GetDetailVendorUsecase(private val repository: VendorRepositoryImpl) {
    suspend fun getVendorDetail():VendorDetail = repository.getVendorDetail()
}