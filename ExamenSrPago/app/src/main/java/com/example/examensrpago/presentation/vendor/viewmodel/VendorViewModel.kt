package com.example.examensrpago.presentation.vendor.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.example.examensrpago.domain.vendor.GetDetailVendorUsecase
import com.example.examensrpago.presentation.base.Resource
import kotlinx.coroutines.Dispatchers

class VendorViewModel(private val getDetailVendorUsecase: GetDetailVendorUsecase):ViewModel() {

    fun getVendorDetailFetch() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(getDetailVendorUsecase.getVendorDetail()))
        }
        catch (ex:Exception){
            emit(Resource.Failure(ex))
        }
    }



    class VendorViewModelFactory(private val getDetailVendorUsecase: GetDetailVendorUsecase):ViewModelProvider.Factory{
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
          return modelClass.getConstructor(GetDetailVendorUsecase::class.java).newInstance(getDetailVendorUsecase)
        }

    }
}